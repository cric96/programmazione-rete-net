package home.model.IP;

import home.model.host.Identifier;
import home.model.packet.Header;

public interface IPHeader extends Header{
	/**
	 * get the source host that want to send the packet
	 * @return
	 * 	the identifier associated with this host
	 */
	Identifier getSourceHost();
	/**
	 * get the destination host
	 * @return
	 * 	the identifier associated with the destination host
	 */
	Identifier getDestinationHost();
}
