package home.model.data;

import home.model.packet.Header;
import home.model.packet.Packet;

public interface DataPacket<H extends Header,P> extends Packet<H, P>{

}
