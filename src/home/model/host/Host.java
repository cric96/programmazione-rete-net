package home.model.host;

import java.util.List;
import java.util.Optional;

import home.model.net.Net;
import home.model.packet.Packet;

/**
 * define a generic host that could be connect to the net or not
 */
public interface Host {
	/**
	 * get the name of the host
	 * @return
	 * 	the name
	 */
	String getName();
	/**
	 * try to connect to net net
	 * @throws IllegalArgumentException 
	 * 	if the net isn't correct
	 * @param net
	 * 	the net used to connect
	 */
	void connect(Net net);
	/**
	 * the host try to disconnect to the current host
	 * @throws IllegalStateExcetion if the host is currently don't connect;
	 */
	void disconnect();
	/**
	 * the id in the net associated with this host
	 * @return
	 * 	Optional of Identifier if the host is connect Optional.empty otherwise 
	 */
	Optional<Identifier> getId();
	/**
	 * 
	 * @return
	 * 	the list of process in running
	 */
	List<Process> getProcesses();
	/**
	 * a packet comes to the host
	 * @param packet
	 * 	the packet received
	 */
	void receivePacket(Packet<?, ?> packet);
}
