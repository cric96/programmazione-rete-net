package home.model.host;
/**
 * is an identifier to define a id to a host
 */
public interface Identifier {
	/**
	 * get the id of the id;
	 */
	int getId();
}
