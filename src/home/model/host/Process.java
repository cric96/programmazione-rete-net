package home.model.host;
/**
 * a process running in a host
 */
public interface Process {
	Identifier getPort();
}
