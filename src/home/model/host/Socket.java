package home.model.host;

import home.model.packet.Packet;

/**
 * a socket that allow to send packet to the net
 *  @param <E>
 *  	the type of packet that the socket sent
 */
public interface Socket <E extends Packet<?,?>> {
	/**
	 * @param packet
	 * 	the packet to send in the net
	 */
	void sendPacket(E packet);
	/**
	 * 
	 * @return
	 * 	the packet received
	 */
	E reicevePacket();
}
