package home.model.net;

import home.model.host.Host;
import home.model.host.Identifier;
import home.model.packet.Packet;

/**
 * a simulation of a net
 */
public interface Net {
	/**
	 * try to send a packet to a host define into the packet
	 * @param packet
	 * 	the packet used to send
	 */
	void sendPacket(Packet<?,?> packet);
	/**
	 * connect a host to the net
	 * @return
	 * 	the identifier associated to this host
	 */
	Identifier connect(Host host);
}
