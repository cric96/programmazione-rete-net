package home.model.net;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import home.model.IP.IPPacket;
import home.model.host.Host;
import home.model.host.Identifier;
import home.model.packet.Packet;

class PerfectNetImpl implements Net {
	private static final int MAX_HOST = 1024;
	private static final String ERR_HOST = "no host connect with this id";
	private static final String ERR_PACK = "i can't send this packet";
	private final Map<Identifier, Host> hostes;
	
	public PerfectNetImpl() {
		hostes = new HashMap<>();
	}
	@Override
	public void sendPacket(Packet<?, ?> packet) {
		Objects.requireNonNull(packet);
		if(packet instanceof IPPacket) {
			final IPPacket packetToSend = (IPPacket)packet;
			Identifier id = packetToSend.getHeader().getDestinationHost();
			if(!this.hostes.containsKey(id)) {
				throw new IllegalArgumentException(ERR_HOST);
			} else {
				final Host destination = this.hostes.get(id);
				destination.receivePacket(packet);
			}
		}
		throw new IllegalArgumentException(ERR_PACK);
	}

	@Override
	public Identifier connect(Host host) {
		Objects.requireNonNull(host);
		int id;
		do {
			id = (int) (Math.random() * MAX_HOST);
		} while(this.hostes.containsKey(new BasicId(id)));
		final Identifier idObj = new BasicId(id);
		this.hostes.put(idObj, host);
		return idObj;
	}
	private static class BasicId implements Identifier {
		private final int id;
		
		BasicId(final int id) {
			this.id = id;
		}
		@Override
		public int getId() {
			return this.id;
		}
		
	}
}	
