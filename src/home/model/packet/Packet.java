package home.model.packet;
/**
 * an interface to define a generic packet
 * @param <H>
 * 	the header of this packet
 * @param <P>
 * 	the data that the packet wrap
 */
public interface Packet <H extends Header, P>{
	/**
	 * get the header of this packet
	 * @return
	 * 	the header 
	 */
	H getHeader();
	/**
	 * get the packet that this packet wrap
	 * @return
	 * 	the packet
	 */
	P getPacket();
}
