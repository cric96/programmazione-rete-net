package home.model.trasport;

import home.model.packet.Header;

/**
 * the header of the transport packet
 *
 */
public interface TransportHeader extends Header{
	/**
	 * the source port of sender
	 * @return
	 * 	the id of the port
	 */
	int getSourcePort();
	/**
	 * the destination port of receiver
	 * @return
	 * 	the id of the port
	 */
	int getDestinationPort();
}
