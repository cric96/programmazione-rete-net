package home.model.trasport;

import home.model.data.DataPacket;
import home.model.packet.Packet;
/**
 * a packet of transport layer
 */
public interface TransportPacket extends Packet<TransportHeader, DataPacket<?, ?>>{
	
}
